#include "../btagAnalysis/TaggerScoreBranches.hh"
#include "../btagAnalysis/TaggerScoreBranchBuffer.hh"

#include "xAODJet/Jet.h"
#include "AthContainers/exceptions.h"
#include "TTree.h"



//!-----------------------------------------------------------------------------------------------------------------------------!//
TaggerScoreBranches::TaggerScoreBranches():
  m_branches(new TaggerScoreBranchBuffer)
{
  // instantiate all the vectors here ...


  m_branches->v_jet_dl1_pb    = new std::vector<double>();
  m_branches->v_jet_dl1_pc    = new std::vector<double>();
  m_branches->v_jet_dl1_pu    = new std::vector<double>();
  m_branches->v_jet_dl1mu_pb  = new std::vector<double>();
  m_branches->v_jet_dl1mu_pc  = new std::vector<double>();
  m_branches->v_jet_dl1mu_pu  = new std::vector<double>();
  m_branches->v_jet_dl1rnn_pb = new std::vector<double>();
  m_branches->v_jet_dl1rnn_pc = new std::vector<double>();
  m_branches->v_jet_dl1rnn_pu = new std::vector<double>();
  m_branches->v_jet_mv2c10    = new std::vector<double>();
  m_branches->v_jet_mv2c10mu  = new std::vector<double>();
  m_branches->v_jet_mv2c10rnn = new std::vector<double>();
  m_branches->v_jet_mv2c100   = new std::vector<double>();
  m_branches->v_jet_mv2cl100  = new std::vector<double>();
}

//!-----------------------------------------------------------------------------------------------------------------------------!//
TaggerScoreBranches::~TaggerScoreBranches() {
  // delete all the vectors here ...
  delete m_branches->v_jet_dl1_pb;
  delete m_branches->v_jet_dl1_pc;
  delete m_branches->v_jet_dl1_pu;
  delete m_branches->v_jet_dl1mu_pb;
  delete m_branches->v_jet_dl1mu_pc;
  delete m_branches->v_jet_dl1mu_pu;
  delete m_branches->v_jet_dl1rnn_pb;
  delete m_branches->v_jet_dl1rnn_pc;
  delete m_branches->v_jet_dl1rnn_pu;
  delete m_branches->v_jet_mv2c10;
  delete m_branches->v_jet_mv2c10mu;
  delete m_branches->v_jet_mv2c10rnn;
  delete m_branches->v_jet_mv2c100;
  delete m_branches->v_jet_mv2cl100;

  delete m_branches;
}

void TaggerScoreBranches::set_tree(TTree& output_tree) const {

  output_tree.Branch( "jet_dl1_pb", &m_branches->v_jet_dl1_pb );
  output_tree.Branch( "jet_dl1_pc", &m_branches->v_jet_dl1_pc );
  output_tree.Branch( "jet_dl1_pu", &m_branches->v_jet_dl1_pu );
  output_tree.Branch( "jet_dl1mu_pb", &m_branches->v_jet_dl1mu_pb );
  output_tree.Branch( "jet_dl1mu_pc", &m_branches->v_jet_dl1mu_pc );
  output_tree.Branch( "jet_dl1mu_pu", &m_branches->v_jet_dl1mu_pu );
  output_tree.Branch( "jet_dl1rnn_pb", &m_branches->v_jet_dl1rnn_pb );
  output_tree.Branch( "jet_dl1rnn_pc", &m_branches->v_jet_dl1rnn_pc );
  output_tree.Branch( "jet_dl1rnn_pu", &m_branches->v_jet_dl1rnn_pu );
  output_tree.Branch( "jet_mv2c10", &m_branches->v_jet_mv2c10 );
  output_tree.Branch( "jet_mv2c10mu", &m_branches->v_jet_mv2c10mu );
  output_tree.Branch( "jet_mv2c10rnn", &m_branches->v_jet_mv2c10rnn );
  output_tree.Branch( "jet_mv2c100", &m_branches->v_jet_mv2c100 );
  output_tree.Branch( "jet_mv2cl100", &m_branches->v_jet_mv2cl100 );

}

//!-----------------------------------------------------------------------------------------------------------------------------!//
void TaggerScoreBranches::fill(const xAOD::Jet& jet) {

    const xAOD::BTagging *bjet = jet.btagging();

    // DL1
    try {
      m_branches->v_jet_dl1_pb->push_back(bjet->auxdata<double>("DL1_pb"));
      m_branches->v_jet_dl1_pc->push_back(bjet->auxdata<double>("DL1_pc"));
      m_branches->v_jet_dl1_pu->push_back(bjet->auxdata<double>("DL1_pu"));
    } catch(...) {
      m_branches->v_jet_dl1_pb->push_back(-99);
      m_branches->v_jet_dl1_pc->push_back(-99);
      m_branches->v_jet_dl1_pu->push_back(-99);
    }

    try {
      m_branches->v_jet_dl1mu_pb->push_back(bjet->auxdata<double>("DL1mu_pb"));
      m_branches->v_jet_dl1mu_pc->push_back(bjet->auxdata<double>("DL1mu_pc"));
      m_branches->v_jet_dl1mu_pu->push_back(bjet->auxdata<double>("DL1mu_pu"));
    } catch(...) {
      m_branches->v_jet_dl1mu_pb->push_back(-99);
      m_branches->v_jet_dl1mu_pc->push_back(-99);
      m_branches->v_jet_dl1mu_pu->push_back(-99);
    }

    try {
      m_branches->v_jet_dl1rnn_pb->push_back(bjet->auxdata<double>("DL1rnn_pb"));
      m_branches->v_jet_dl1rnn_pc->push_back(bjet->auxdata<double>("DL1rnn_pc"));
      m_branches->v_jet_dl1rnn_pu->push_back(bjet->auxdata<double>("DL1rnn_pu"));
    } catch(...) {
      m_branches->v_jet_dl1rnn_pb->push_back(-99);
      m_branches->v_jet_dl1rnn_pc->push_back(-99);
      m_branches->v_jet_dl1rnn_pu->push_back(-99);
    }

    try {
      m_branches->v_jet_mv2c10->push_back(bjet->auxdata<double>("MV2c10_discriminant"));
    } catch(...) {
      m_branches->v_jet_mv2c10->push_back(-99);
    }
    try {
      m_branches->v_jet_mv2c10mu->push_back(bjet->auxdata<double>("MV2c10mu_discriminant"));
    } catch(...) {
      m_branches->v_jet_mv2c10mu->push_back(-99);
    }
    try {
      m_branches->v_jet_mv2c10rnn->push_back(bjet->auxdata<double>("MV2c10rnn_discriminant"));
    } catch(...) {
      m_branches->v_jet_mv2c10rnn->push_back(-99);
    }

    try {
      m_branches->v_jet_mv2c100->push_back(bjet->auxdata<double>("MV2c100_discriminant"));
    } catch(...) {
      m_branches->v_jet_mv2c100->push_back(-99);
     }
    try {
      m_branches->v_jet_mv2cl100->push_back(bjet->auxdata<double>("MV2cl100_discriminant"));
    } catch(...) {
      m_branches->v_jet_mv2cl100->push_back(-99);
    }


}

//!-----------------------------------------------------------------------------------------------------------------------------!//
void TaggerScoreBranches::clear() {
  // clear vectors

  m_branches->v_jet_dl1_pb->clear();
  m_branches->v_jet_dl1_pc->clear();
  m_branches->v_jet_dl1_pu->clear();
  m_branches->v_jet_dl1mu_pb->clear();
  m_branches->v_jet_dl1mu_pc->clear();
  m_branches->v_jet_dl1mu_pu->clear();
  m_branches->v_jet_dl1rnn_pb->clear();
  m_branches->v_jet_dl1rnn_pc->clear();
  m_branches->v_jet_dl1rnn_pu->clear();
  m_branches->v_jet_mv2c10->clear();
  m_branches->v_jet_mv2c10mu->clear();
  m_branches->v_jet_mv2c10rnn->clear();
  m_branches->v_jet_mv2c100->clear();
  m_branches->v_jet_mv2cl100->clear();


}





